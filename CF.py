
import numpy as np
import pandas as pd
import utils
from data import DataSet
import datetime

class CollaborativeFiltering:

    MAX_RATING = 5
    MIN_RATING = 1

    def __init__(self, df_matrix):

        self.matrix = df_matrix

        # get pearson distance between users
        # shape of this matrix = (users,users)
        similarity_matrix = self.matrix.T.corr()
        # fill NA with 0 values
        self.similarity_matrix = similarity_matrix.fillna(0)

    def predict(self, test_indexes):
        # np.newaxis - increase the dimension of the existing array by one more dimension, when used once
        # formula from pdf
        return self.calculate_cb(self.similarity_matrix, self.matrix, test_indexes)

    @staticmethod
    def calculate_cb(similarity_matrix, matrix, test_indexes):
    # similarity_matrix - similarity matrix with zeroes on NaN indexes(dataFrame)
    # matrix - utility matrix (dataFrame)

        mean = matrix.mean(axis=1).values
        matrix = matrix.values
        similarity_matrix = similarity_matrix.values

        res_matrix = np.zeros(matrix.shape)

        # i - user index
        # j - movie index
        for i, j in zip(test_indexes[0], test_indexes[1]):
            k = 0
            s = 0
            for user in range(res_matrix.shape[0]):
                curr_sim_val = similarity_matrix[i][user]
                curr_rating = matrix[user][j]
                if  np.isnan(curr_rating) or np.isnan(curr_sim_val) or user == i:
                    continue
                k += abs(curr_sim_val)
                s += curr_sim_val * (curr_rating - mean[user])

            if k != 0:
                res_matrix[i][j] = mean[i] + s/k
                if res_matrix[i][j] > CollaborativeFiltering.MAX_RATING:
                    res_matrix[i][j] = CollaborativeFiltering.MAX_RATING
                elif res_matrix[i][j] < CollaborativeFiltering.MIN_RATING:
                    res_matrix[i][j] = CollaborativeFiltering.MIN_RATING
            else:
                res_matrix[i][j] = mean[i]

        return res_matrix
