
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from scipy import sparse
from ast import literal_eval

from sklearn.metrics.pairwise import linear_kernel
from sklearn.metrics.pairwise import cosine_similarity



import utils
from data import DataSet
from imdb_movies import ImdbFeatures


class ContentBased:

    def __init__(self, df_matrix, features):
        self.utility_matrix = df_matrix
        self.mean = df_matrix.mean(axis=1).values
        self.features = features

    def predict(self, test_indexes):
        ds = DataSet()
        for feature in self.features:
            ds.metadata[feature] = ds.metadata[feature].apply(literal_eval)
            ds.metadata[feature] = ds.metadata[feature].apply(self.clean_data)

        ds.metadata['soup'] = ds.metadata.apply(self.create_soup, axis=1)

        tf = TfidfVectorizer(analyzer='word', min_df=0, stop_words='english')

        tfidf_matrix = tf.fit_transform(ds.metadata['soup'])

        user_matrix = self.create_vectors_of_users(tfidf_matrix)

        return self.update_matrix_with_grades(user_matrix, tfidf_matrix, test_indexes)


    def create_vectors_of_users(self, tfidf_matrix):
        
        user_matrix  = sparse.dok_matrix((self.utility_matrix.shape[0], tfidf_matrix.shape[1]))

        count_matrix = sparse.dok_matrix((self.utility_matrix.shape[0], tfidf_matrix.shape[1]))

        utility_matrix = self.utility_matrix.fillna(0).values

        non_zeroes_idx = utility_matrix.nonzero()
        utility_matrix = utility_matrix.tolist()
        for utility_user, film in zip(non_zeroes_idx[0], non_zeroes_idx[1]):
            matrix_per_user = tfidf_matrix[film]
            curr_grade = utility_matrix[utility_user][film] - self.mean[utility_user]

            for idx_feature in matrix_per_user.indices:
                user_matrix[utility_user, idx_feature] += curr_grade
                count_matrix[utility_user, idx_feature] += 1

        non_zeroes_user_idx = user_matrix.nonzero()

        for user, feature in zip(non_zeroes_user_idx[0], non_zeroes_user_idx[1]):
            user_matrix[user, feature] /= count_matrix[user, feature]

        return user_matrix

    def update_matrix_with_grades(self, user_matrix, tfidf_matrix, test_indexes):

        c_similarity = cosine_similarity(user_matrix, tfidf_matrix)

        res_matrix = np.zeros(self.utility_matrix.shape)

        # i - user index
        # j - movie index
        for i, j in zip(test_indexes[0], test_indexes[1]):
            res_matrix[i][j] = c_similarity[i, j] + self.mean[i]

        return res_matrix

    def create_soup(self, df):
        res = ''
        for feature in self.features:
            res += ' '.join(df[feature]) + ' '
        res = res[:-1]
        return res

    @staticmethod
    def clean_data(x):
        if isinstance(x, list):
            return [str.lower(i.replace(" ", "")) for i in x]
        else:
            if isinstance(x, str):
                return str.lower(x.replace(" ", ""))
            else:
                return ''
