import pandas as pd
from imdb_movies import ImdbFeatures

# This code should be run for 800 links only, otherwise the runtime error can occur(IMDB block for ddos)

class MetaData():

    @staticmethod
    def split_genres(x):
        return x.split("|")

    def build_meta_data(self):
        features = ImdbFeatures().get_full_data_frame()
        features.to_csv('metadata.csv', sep=',', index=False)

    def build_final_meta_data(self):
        movies = pd.read_csv('movies.csv', usecols=['movieId', 'title', 'genres'])
        metadata = pd.read_csv('metadata.csv', usecols=['movieId','imdbId','directors','writers','stars','countries','languages'])

        movies['genres'] = movies['genres'].apply(self.split_genres)
        movies['genres'] = movies['genres'].fillna("")

        final_metadata = pd.merge(movies, metadata)
        final_metadata.to_csv('final_metadata.csv', sep=',',index=False)


if __name__ == 'main':

    meta_data = MetaData()

    meta_data.build_meta_data()

    meta_data.build_final_meta_data()
