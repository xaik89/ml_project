
import numpy as np
from scipy.sparse.linalg import svds


class SVD:

    def __init__(self, df_matrix):
        self.matrix = df_matrix

        # average ratings that all users give to specific movie
        self.users_mean = df_matrix.mean(axis=1)
        self.movies_mean = df_matrix.mean(axis=0)

    def predict_simple_SVD(self):
        # u represents user vector,
        # s represents the item vector.
        # vt represent the joint of these two vectors as collection of points (ie vector) in 2 dimensional spaces.

        normalized_matrix = self.matrix.sub(self.users_mean, axis=0).fillna(0)

        u, s, vt = svds(normalized_matrix, k=10)
        s_diag_matrix = np.diag(s)

        return np.dot(np.dot(u, s_diag_matrix), vt) + self.users_mean.values.reshape(-1, 1)

    def predict_SGD_SVD(self):
        # Preprocessing
        normalized_matrix = self.matrix.sub(self.users_mean, axis=0).fillna(0)

        # optimization of the elements of U and V
        U, V = self.SGD(self.matrix, normalized_matrix.values)

        # return predicted matrix
        return np.dot(U, V.T) + self.users_mean.values.reshape(-1, 1)

    @staticmethod
    def SGD(init_matrix, normalized_matrix):
        # Learn the vectors p_u and q_i with SGD.

        # Hyper-parameters
        n_factors = 10  # number of factors
        alpha = 0.01  # learning rate
        n_epochs = 10  # number of iteration of the SGD procedure

        # Randomly initialize the user and item factors.
        p = np.random.normal(0, .1, (normalized_matrix.shape[0], n_factors))
        q = np.random.normal(0, .1, (normalized_matrix.shape[1], n_factors))

        non_NaN_indexes = np.argwhere(init_matrix.fillna(0).values)

        # Optimization procedure
        for _ in range(n_epochs):
            for u, i in non_NaN_indexes:
                err = normalized_matrix[u][i] - np.dot(p[u], q[i])
                # Update vectors p_u and q_i
                p[u] += alpha * err * q[i]
                q[i] += alpha * err * p[u]

        return p, q
