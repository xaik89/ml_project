
from sklearn.metrics import mean_squared_error
from math import sqrt

import numpy as np
import pandas as pd

# calculate RMSE for two sparse matrix
def rmse(prediction, test):
    # ignore nonzero values
    # 2d arrays -> 1d array with flatten

    indices = test.nonzero()
    prediction = prediction[indices].flatten()
    test = test[indices].flatten()

    return sqrt(mean_squared_error(test, prediction))

def build_similarity_matrix(matrix):
    # return matrix of similarity between users
    # with cosine distance
    # same as : np.corrcoef(matrix)
    return np.corrcoef(matrix)

