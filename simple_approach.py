
import numpy as np
import pandas as pd

class SimpleApproach:
    def __init__(self, df_matrix):
        self.matrix = df_matrix

    @staticmethod
    def get_average_values_on_matrix(utility_matrix):
        # average ratings that user gives to all seen movies
        user_mean = utility_matrix.mean(axis=1)

        # average ratings that all users give to specific movie
        movie_mean = utility_matrix.mean(axis=0)

        # # calculate matrix with all means(each value)
        zero_mat = utility_matrix.fillna(0) - utility_matrix.fillna(0)
        avg_matrix = (user_mean[:, np.newaxis] + zero_mat + movie_mean[np.newaxis:].fillna(0))/2
        return avg_matrix

    def predict(self):
        return self.get_average_values_on_matrix(self.matrix).values

