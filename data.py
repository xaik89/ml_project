import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from collections import Counter
import numpy as np

from imdb_movies import ImdbFeatures

class DataSet:
    def __init__(self):
        self.movies = pd.read_csv('movies.csv', usecols=['movieId', 'title', 'genres'])

        self.ratings = pd.read_csv('ratings.csv', usecols=['userId', 'movieId', 'rating'])

        self.merged = pd.merge(self.movies, self.ratings)

        self.metadata = pd.read_csv('final_metadata.csv', usecols=['movieId','title','genres','imdbId','directors','writers','stars','countries','languages'])

    def print_sparcity(self):
        n_u = len(self.ratings["userId"].unique())
        n_m = len(self.ratings["movieId"].unique())
        sparsity = len(self.ratings)/(n_u * n_m)
        print("sparsity of ratings is %.2f%%" %(sparsity * 100))

    def distribution(self):
        sns.set_style('whitegrid')
        sns.set(font_scale=1.5)

        # Display distribution of rating
        self.ratings.hist(column='rating')

        plt.show()

    # Get summary statistics of rating
    def get_summary_statistics_of_rating(self):
        print(self.ratings['rating'].mean())
        self.distribution()

    def get_popularity_of_genres(self):
        dict_of_genres = Counter()

        for _, row in self.merged.iterrows():
            list_of_genres = row['genres'].split('|')
            for genre in list_of_genres:
                like_to_film = 0
                if row['rating'] > 3:
                    like_to_film = 1
                elif row['rating'] < 3:
                    like_to_film = -1
                dict_of_genres[genre] += like_to_film

        sns.set_style('whitegrid')
        sns.set(font_scale=1.5)

        # get top 5 from dict_of_genres
        top_genre_dict = dict(Counter(dict_of_genres).most_common(5))

        plt.bar(top_genre_dict.keys(), top_genre_dict.values(), color='g')
        plt.show()


if __name__ == 'main':
    ds = DataSet()
    # print mean and plot of distribution of ratings
    ds.get_summary_statistics_of_rating()

    # example of merged data frame(built with panda via inner join)
    print(ds.merged.head())

    # print plot of 5 most popular genres
    ds.get_popularity_of_genres()

    # print sparcity of our utility matrix of ratings.
    ds.print_sparcity()

