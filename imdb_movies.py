
import pandas as pd

import asyncio
from aiohttp import ClientSession, TCPConnector

from bs4 import BeautifulSoup


class FetchUrlsAsync:
    """
    asynchronous requests to get list of urls content
    support only for python 3.5+!
    """
    def __init__(self, urls):
        self.responses = None

        loop = asyncio.get_event_loop()
        future = asyncio.ensure_future(self.run(urls))
        loop.run_until_complete(future)

    def get_response(self):
        return self.responses

    @staticmethod
    async def fetch(url, session):
        async with session.get(url) as response:
            return await response.read()

    async def run(self, ulrs):
        tasks = []

        # Fetch all responses within one Client session,
        # keep connection alive for all requests.
        async with ClientSession(connector=TCPConnector(verify_ssl=False)) as session:
            for url in ulrs:
                task = asyncio.ensure_future(self.fetch(url, session))
                tasks.append(task)

            # all outputs(responses) here
            self.responses = await asyncio.gather(*tasks)


class ImdbFeatures:

    def get_full_data_frame(self):
        """build new dataframe with links.csv and features from IMDB(web scrapping)

        Returns:
            dataframe with most important features of the movies
        """

        links = pd.read_csv('links.csv',  usecols=['movieId', 'imdbId'], converters={'imdbId': lambda x: str(x)})

        # list of urls in imdb, num is string.
        urls = ['http://www.imdb.com/title/tt' + num + '/' for num in links['imdbId']]

        # call to get responses from all urls (asynchronous)
        async_urls = FetchUrlsAsync(urls)

        dict_of_features = self.get_main_features(async_urls.get_response())

        # where place a new column in df
        index_column = 2
        for k, v in dict_of_features.items():
            se = pd.Series(v)
            links.insert(loc=index_column, column=k, value=se.values)
            index_column += 1

        return links

    def get_main_features(self, response_list):
        """
        build from list of responses(imdb pages) a dict of list of features
        """

        f_dict = {'directors':list(), 'writers':list(), 'stars':list(), 'countries':list(), 'languages':list()}

        for response in response_list:
                soup = BeautifulSoup(response, "html.parser")
                try:
                    f_dict['directors'].append([tag.get_text() for tag in soup.find('h4', string=["Director:","Directors:"]).find_next_siblings('a', limit=2)])
                except:
                    f_dict['directors'].append([])
                try:
                    f_dict['writers'].append([tag.get_text() for tag in soup.find('h4', string=["Writer:","Writers:"]).find_next_siblings('a', limit=2)])
                except:
                    f_dict['writers'].append([])
                try:
                    f_dict['stars'].append([tag.get_text() for tag in soup.find('h4', string=["Star:","Stars:"]).find_next_siblings('a', limit=3)])
                except:
                    f_dict['stars'].append([])
                try:
                    f_dict['countries'].append([tag.get_text() for tag in soup.find('h4', string="Country:").find_next_siblings('a', limit=2)])
                except:
                    f_dict['countries'].append([])
                try:
                    f_dict['languages'].append([tag.get_text() for tag in soup.find('h4', string="Language:").find_next_siblings('a', limit=2)])
                except:
                    f_dict['languages'].append([])
        return f_dict



if __name__ == 'main':

    ImdbFeatures().get_full_data_frame()

