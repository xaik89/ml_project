from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_validate


import utils
import pandas as pd
import numpy as np

from data import DataSet
from simple_approach import SimpleApproach
from content_based import ContentBased
from CF import CollaborativeFiltering
from SVD import SVD


ds = DataSet()
df = ds.ratings

train = df.reset_index(                  # need to keep the index as a column
    ).groupby('userId'                   # split by "userId"
    ).apply(lambda x: x.sample(frac=0.8) # in each group, do the random split
    ).reset_index(drop=True              # index now is group id - reset it
    ).set_index('index')                 # reset the original index
test = df.drop(train.index)

df_m = df.pivot(index='userId', columns='movieId', values='rating')
train_matrix = train.pivot(index='userId', columns='movieId', values='rating').reindex(index=df_m.index, columns=df_m.columns)
test_matrix = test.pivot(index='userId', columns='movieId', values='rating').reindex(index=df_m.index, columns=df_m.columns)

# simple matrixes
random_matrix = np.random.uniform(low=1, high=5, size=df_m.shape)
zero_mat = train_matrix.fillna(0) - train_matrix.fillna(0)

# convert df to numpy array
test_matrix = test_matrix.fillna(0).values

simple_prediction = SimpleApproach(train_matrix.copy()).predict()

cb_prediction1 = ContentBased(train_matrix.copy(),['genres']).predict(test_matrix.nonzero())

#cb_prediction2 = ContentBased(train_matrix.copy(),['directors']).predict(test_matrix.nonzero())

#cb_prediction3 = ContentBased(train_matrix.copy(),['writers']).predict(test_matrix.nonzero())

#cb_prediction4 = ContentBased(train_matrix.copy(),['stars']).predict(test_matrix.nonzero())

#cb_prediction5 = ContentBased(train_matrix.copy(),['countries']).predict(test_matrix.nonzero())

cb_prediction6 = ContentBased(train_matrix.copy(),['genres','countries']).predict(test_matrix.nonzero())


user_prediction = CollaborativeFiltering(train_matrix.copy()).predict(test_matrix.nonzero())

simple_svd_prediction = SVD(train_matrix.copy()).predict_simple_SVD()

svd_sgd_prediction = SVD(train_matrix.copy()).predict_SGD_SVD()

print("\n\t Tests all approaches with RMSE\n")

print('\tRandom approach:   {}\n'.format(utils.rmse(random_matrix, test_matrix)))

print('\t3 in NaN indexes:  {}\n'.format(utils.rmse(train_matrix.fillna(3).values, test_matrix)))

print('\tMean of users:     {}\n'.format(utils.rmse(zero_mat.values + train_matrix.mean(axis=1).values.reshape(-1, 1), test_matrix)))

print('\tMean of films:     {}\n'.format(utils.rmse((zero_mat + train_matrix.mean(axis=0)[np.newaxis:].fillna(0)).values, test_matrix)))

print('\tSimple approach:   {}\n'.format(utils.rmse(simple_prediction, test_matrix)))

print('\tContent genres based approach: {}\n'.format(utils.rmse(cb_prediction1, test_matrix)))

#print('\tContent directors based approach: {}\n'.format(utils.rmse(cb_prediction2, test_matrix)))

#print('\tContent writers based approach: {}\n'.format(utils.rmse(cb_prediction3, test_matrix)))

#print('\tContent stars based approach: {}\n'.format(utils.rmse(cb_prediction4, test_matrix)))

#print('\tContent countries based approach: {}\n'.format(utils.rmse(cb_prediction5, test_matrix)))

print('\tContent soup based approach: {}\n'.format(utils.rmse(cb_prediction6, test_matrix)))

print('\tUser-based CF:     {}\n'.format(utils.rmse(user_prediction, test_matrix)))

print('\tsimple SVD CF:     {}\n'.format(utils.rmse(simple_svd_prediction, test_matrix)))

print('\tSVD_SGD CF:        {}\n'.format(utils.rmse(svd_sgd_prediction, test_matrix)))

